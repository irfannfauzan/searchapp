class ModelGithub {
  final int? total;
  final bool? incomp;
  final List<Items>? items;

  ModelGithub({this.total, this.incomp, this.items});

  factory ModelGithub.fromJson(Map<String, dynamic> json) => ModelGithub(
      total: json['total_count'],
      incomp: json['incomplete_results'],
      items: List.from(json['items']).map((e) => Items.fromJson(e)).toList());
}

class Items {
  final String? nama;
  final Owner? owner;

  Items({this.nama, this.owner});
  factory Items.fromJson(Map<String, dynamic> json) =>
      Items(nama: json['name'], owner: Owner.fromJson(json['owner']));
}

class Owner {
  final String? login;
  final String? avatar;
  Owner({this.login, this.avatar});

  factory Owner.fromJson(Map<String, dynamic> json) =>
      Owner(avatar: json['avatar_url'], login: json['login']);
}
