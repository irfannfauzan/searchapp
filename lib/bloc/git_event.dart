part of 'git_bloc.dart';

abstract class GitEvent extends Equatable {
  const GitEvent();

  @override
  List<Object> get props => [];
}

class FetchGit extends GitEvent {
  final String query;

  FetchGit(this.query);
  @override
  List<Object> get props => [];
}
