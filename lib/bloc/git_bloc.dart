import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:search/model.dart';
import 'package:search/service.dart';

part 'git_event.dart';
part 'git_state.dart';

class GitBloc extends Bloc<GitEvent, GitState> {
  GitBloc() : super(GitInitial()) {
    on<GitEvent>((event, emit) async {
      Services services = Services();
      if (event is FetchGit) {
        final res = await services.fetchGit(event.query);
        emit(SuccesfullyState(res, event.query));
      }
    });
  }
}
