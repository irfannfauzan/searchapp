part of 'git_bloc.dart';

abstract class GitState extends Equatable {
  const GitState();

  @override
  List<Object> get props => [];
}

class GitInitial extends GitState {
  @override
  List<Object> get props => [];
}

class SuccesfullyState extends GitState {
  final ModelGithub modelGithub;
  final String query;

  SuccesfullyState(this.modelGithub, this.query);

  @override
  List<Object> get props => [];
}
