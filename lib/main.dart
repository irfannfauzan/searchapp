import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:search/bloc/git_bloc.dart';

void main() {
  runApp(MyApp());
}

TextEditingController nameSearch = TextEditingController();
String query = nameSearch.text;

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: MultiBlocProvider(providers: [
          BlocProvider(create: (context) => GitBloc()),
        ], child: MyHome()));
  }
}

class MyHome extends StatefulWidget {
  const MyHome({Key? key}) : super(key: key);

  @override
  _MyHomeState createState() => _MyHomeState();
}

class _MyHomeState extends State<MyHome> {
  @override
  void initState() {
    context.read<GitBloc>().add(FetchGit(query));
    super.initState();
  }

  GitBloc _bloc = GitBloc();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: Column(children: [
        SizedBox(
          height: 10,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 40, right: 40),
          child: Container(
              height: 50,
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
              width: double.infinity,
              decoration: BoxDecoration(
                  color: Color(0xFFD4D8DE),
                  borderRadius: BorderRadius.circular(4),
                  border: Border.all(color: Color(0xFFF5F5F5))),
              child: BlocBuilder<GitBloc, GitState>(
                builder: (context, state) {
                  if (state is SuccesfullyState) {
                    return TextField(
                      controller: nameSearch,
                      decoration: InputDecoration(
                          suffixIcon: IconButton(
                            onPressed: () {
                              setState(() {
                                query = nameSearch.text;
                                _bloc.add(FetchGit(query));
                                print(query);
                              });
                            },
                            icon: Icon(
                              Icons.check,
                              color: Colors.green,
                            ),
                          ),
                          hintText: "Search",
                          icon: Icon(
                            Icons.search,
                            color: Colors.green,
                          ),
                          border: InputBorder.none),
                    );
                  } else {
                    return CircularProgressIndicator();
                  }
                },
              )),
        ),
        SizedBox(
          height: 10,
        ),
        BlocBuilder<GitBloc, GitState>(builder: (context, state) {
          if (state is SuccesfullyState) {
            final result = state.modelGithub.items;
            return Flexible(
              child: ListView.builder(
                  itemCount: result!.length,
                  itemBuilder: (context, index) {
                    final res = result[index];
                    return Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 16.0, right: 16),
                          child: Container(
                            width: double.infinity,
                            height: 78,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color: Color(0xFFF4F5F9),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                        width: 72,
                                        height: 75,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(8),
                                            image: DecorationImage(
                                                image: NetworkImage(res
                                                    .owner!.avatar
                                                    .toString()),
                                                fit: BoxFit.cover))),
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(left: 12.0),
                                          child: Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                2,
                                            child: Text(
                                              res.nama.toString(),
                                              style: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 14,
                                              ),
                                              maxLines: 2,
                                              overflow: TextOverflow.clip,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              left: 12.0, top: 26),
                                          child: Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                2,
                                            child: Text(
                                              res.owner!.login.toString(),
                                              style: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 12,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                      ],
                    );
                  }),
            );
          } else {
            return Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CircularProgressIndicator(
                    color: Colors.green,
                  )
                ],
              ),
            );
          }
        })
      ]),
    ));
  }
}
