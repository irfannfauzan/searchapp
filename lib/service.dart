import 'dart:convert';

import 'package:search/model.dart';
import 'package:http/http.dart' as http;

class Services {
  String url = 'https://api.github.com/search/repositories?q=';
  Future<ModelGithub> fetchGit(String query) async {
    if (query == '') {
      print("Query masih kosong");
      query = "Doraemon";
    } else {
      print("Query Updated");
      query = query;
    }
    try {
      final response = await http.get(Uri.parse(url + query));
      print(url + query);
      if (response.statusCode == 200) {
        final result = jsonDecode(response.body);
        return ModelGithub.fromJson(result);
      } else {
        print('err');
      }
    } catch (e) {
      throw Exception(e);
    }
    return ModelGithub();
  }
}
